# Loja Digital WebResponse #

npm install

composer install

### Ferramentas ###

* linguagem PHP
* doctrine orm
* banco de dados Mysql
* Padrão MVC
* [git clone](https://barbearia-moderna-admin@bitbucket.org/barbearia-moderna/loja-ecommerce-digital.git)

## CONFIGURAÇÃO DE IMAGENS

* carrossel = 450x1980
* card-produto = 200x220
* logo = 60x60

### Comandos para atualizações e criações pelo Doctrine

Quando modificar a classe Entity e atualizar o banco
> vendor/bin/doctrine  orm:schema-tool:update --force 